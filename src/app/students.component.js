"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Student = (function () {
    function Student(name, group) {
        this.name = name;
        this.group = group;
        this.isEdited = false;
    }
    return Student;
}());
exports.Student = Student;
var StudentsComponent = (function () {
    function StudentsComponent() {
    }
    StudentsComponent.prototype.ngAfterViewInit = function () {
        var localItems = localStorage.getItem("students");
        if (localItems == null || localItems == undefined || localItems.length <= 0 || localItems == "[]") {
            this.students =
                [
                    { name: "Jack Daniels", isEdited: false, group: 101 },
                    { name: "Paul Armstrong", isEdited: false, group: 101 },
                    { name: "Harry Marder", isEdited: false, group: 102 },
                    { name: "Ronald Hesly", isEdited: false, group: 103 }
                ];
        }
        else {
            this.students = JSON.parse(localItems);
            console.log([localStorage.getItem("students")]);
        }
    };
    StudentsComponent.prototype.addItem = function (name, group) {
        if (name == null || name == undefined || name.trim() == "")
            return;
        if (group == null || group == undefined)
            return;
        this.students.push(new Student(name, group));
        console.log(this.students);
        localStorage.setItem("students", JSON.stringify(this.students));
    };
    StudentsComponent.prototype.deleteItem = function (item) {
        if (item == null || item == undefined)
            return;
        this.students.splice(this.students.indexOf(item), 1);
        localStorage.setItem("students", JSON.stringify(this.students));
    };
    StudentsComponent.prototype.editItem = function (item) {
        item.isEdited = false;
        if (item == null || item == undefined)
            return;
        console.log(item);
        localStorage.setItem("students", JSON.stringify(this.students));
    };
    StudentsComponent = __decorate([
        core_1.Component({
            selector: 'purchase-app',
            templateUrl: 'app/students.html'
        }), 
        __metadata('design:paramtypes', [])
    ], StudentsComponent);
    return StudentsComponent;
}());
exports.StudentsComponent = StudentsComponent;
//# sourceMappingURL=students.component.js.map