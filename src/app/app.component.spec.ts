/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { StudentsComponent } from './students.component';
import { TeachersComponent } from './teachers.component';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';


describe('AppComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [RouterTestingModule]
    });
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.whenStable().then(() => {
      expect(true).toBe(true);
    });
  }));

});