/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { StudentsComponent } from './students.component';
import { AppComponent } from './app.component';
import { TeachersComponent } from './teachers.component';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule }   from '@angular/forms';


describe('StudentsComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StudentsComponent],
      imports: [RouterTestingModule, FormsModule]
    });
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(StudentsComponent);
    fixture.whenStable().then(() => {
      expect(true).toBe(true);
    });
  }));

});