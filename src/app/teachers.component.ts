import { Component } from '@angular/core';

export class Teacher{
    name: string;
    degree: string;
    isEdited: boolean;
     
    constructor(name: string, degree: string) {
  
        this.name = name;
        this.degree = degree;
        this.isEdited = false;  
    }
}


@Component({
    selector: 'purchase-app',
    templateUrl: './teachers.html'
})
export class TeachersComponent { 
    teachers: Teacher[];

    ngAfterViewInit(){

        let localItems = localStorage.getItem("teachers");

        
        if(localItems==null || localItems==undefined || localItems.length<=0 || localItems=="[]"){
    
            this.teachers =
            [
                { name: "Jack Daniels", isEdited: false, degree: "doctorate" },
                { name: "Paul Armstrong", isEdited: false, degree: "doctorate" },
                { name: "Harry Marder", isEdited: false, degree: "professor" },
                { name: "Ronald Hesly", isEdited: false, degree: "doctorate" }
            ];
        }
        else{
           this.teachers = JSON.parse(localItems);
            
        }
    }

    addItem(name: string, degree: string): void {
         
        if(name==null || name==undefined || name.trim()=="")
            return;
        if(degree==null || degree==undefined || degree.trim()=="")
            return;
        this.teachers.push(new Teacher(name, degree));
        localStorage.setItem("teachers", JSON.stringify(this.teachers));

    }

    deleteItem(item): void {

        if(item==null || item==undefined)
            return;
        this.teachers.splice(this.teachers.indexOf(item), 1);
        localStorage.setItem("teachers", JSON.stringify(this.teachers));
    }



    editItem(item): void {
        item.isEdited = false;

        if(item==null || item==undefined)
            return;
        localStorage.setItem("teachers", JSON.stringify(this.teachers));
    }
}