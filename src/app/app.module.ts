import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students.component';
import { TeachersComponent } from './teachers.component';
import {Routes, RouterModule} from '@angular/router';
const appRoutes: Routes =[
    { path: '', component: AppComponent},
    { path: 'students', component: StudentsComponent},
    { path: 'teachers', component: TeachersComponent},
    { path: '**', redirectTo: '/'}
];
@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    TeachersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
