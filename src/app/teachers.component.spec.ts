/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { TeachersComponent } from './teachers.component';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule }   from '@angular/forms';


describe('TeachersComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeachersComponent],
      imports: [RouterTestingModule, FormsModule]
    });
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(TeachersComponent);
    fixture.whenStable().then(() => {
      expect(true).toBe(true);
    });
  }));

});