import { Component } from '@angular/core';

export class Student{
    name: string;
    group: number;
    isEdited: boolean;
     
    constructor(name: string, group: number) {
  
        this.name = name;
        this.group = group;
        this.isEdited = false;  
    }
}


@Component({
    selector: 'purchase-app',
    templateUrl: './students.html'
})
export class StudentsComponent{ 

    students: Student[];

    ngAfterViewInit(){

        let localItems = localStorage.getItem("students");
        
        if(localItems==null || localItems==undefined || localItems.length<=0 || localItems=="[]"){
            this.students =
            [
                { name: "Jack Daniels", isEdited: false, group: 101 },
                { name: "Paul Armstrong", isEdited: false, group: 101 },
                { name: "Harry Marder", isEdited: false, group: 102 },
                { name: "Ronald Hesly", isEdited: false, group: 103 }
            ];
        }
        else{
           this.students = JSON.parse(localItems);
            
        }
    }

    addItem(name: string, group: number): void {
         
        if(name==null || name==undefined || name.trim()=="")
            return;
        if(group==null || group==undefined)
            return;
        this.students.push(new Student(name, group));
        localStorage.setItem("students", JSON.stringify(this.students));

    }

    deleteItem(item): void {

        if(item==null || item==undefined)
            return;
        this.students.splice(this.students.indexOf(item), 1);
        localStorage.setItem("students", JSON.stringify(this.students));
    }



    editItem(item): void {
        item.isEdited = false;

        if(item==null || item==undefined)
            return;
        localStorage.setItem("students", JSON.stringify(this.students));
    }
  
}