import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';


@Component({
    selector: 'app-root',
    template: `<div class="main">
        <router-outlet></router-outlet>
        </div>`
})
export class AppComponent{}

@Component({
  selector: 'form-validation',
  template : `
    <h1>Form Validation</h1>
  `
})
export class FormValidationComponent {

  complexForm : FormGroup;

  constructor(fb: FormBuilder){
    this.complexForm = fb.group({
	  'firstName' : [null, Validators.required],
      'lastName': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])],
      'gender' : [null, Validators.required],
    })
  }
}
