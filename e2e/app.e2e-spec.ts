import { AngularStmPage } from './app.po';

describe('angular-stm App', function() {
  let page: AngularStmPage;

  beforeEach(() => {
    page = new AngularStmPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
